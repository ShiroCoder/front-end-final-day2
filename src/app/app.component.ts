import { Component } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'Shipment Management';
  isLogged = false;
  inNeedRegister: any;
  fullName: string;
  constructor(private router: Router){

  }
    onActivate(componentReference) {
        this.title = componentReference.title;
        this.checkLogin();
        this.registerReq();
        $('meta[name=token]').attr('content', localStorage.getItem('token'));
    }

  ngOnInit() {
    this.checkLogin();
  }

  checkLogin() {
    console.log(this.inNeedRegister);
    this.isLogged = localStorage.getItem('email') === null ? false : true;

    this.fullName = localStorage.getItem('fullName');
    if (this.isLogged === false) {
      this.router.navigate(['/login']);
    }
    if (this.isLogged === false && this.inNeedRegister !== undefined) {
      this.router.navigate(['/register']);
    }

  }

  logout(){
  localStorage.removeItem('email');
  localStorage.removeItem('fullName');
  localStorage.removeItem('token');
  this.isLogged = false;
  this.router.navigate(['/login']);

  }

  registerReq(){
    this.inNeedRegister = 1;
  }
}
