import { ShipmentService } from './../shipment.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  user: any;
  error = false;
  messenger: string;


  constructor(private formBuilder: FormBuilder, private shipmentService: ShipmentService, private router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
  }

      get f() { return this.loginForm.controls; }



       onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.user = {
          email: this.loginForm.get('email').value,
          password: this.loginForm.get('password').value

        }

        this.shipmentService.sign_in(this.user)
            .pipe(first())
            .subscribe(
                data => {
                  localStorage.setItem('email', data['email']);
                  localStorage.setItem('fullName', data['fullName']);
                  localStorage.setItem('token', data['token']);
                  this.router.navigate(['/add']);
                  this.messenger = `Sign-in successfully!`;

                },
                error => {
                    this.error = true;
                    this.messenger = `Password or email is invalid!`;

                });
    }
}



