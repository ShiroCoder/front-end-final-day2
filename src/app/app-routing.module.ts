import { RegisterComponent } from './register/register.component';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './component/list/list.component';
import { AddComponent } from './component/add/add.component';
import { GetComponent } from './component/get/get.component';
const routes: Routes = [
  { path: '', redirectTo: 'add', pathMatch: 'full' },
  { path: 'list', component: ListComponent },
  { path: 'add', component: AddComponent },
  { path: 'get/:id', component: GetComponent},
  { path: 'login', component: LoginComponent },
  {path:  'about', component: AboutComponent},
  {path: 'register', component: RegisterComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
