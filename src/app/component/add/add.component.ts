import { ShipmentService } from './../../shipment.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {


  constructor(private formBuilder: FormBuilder, private shipmentService: ShipmentService, private router: Router) { }


  get f() { return this.form.controls; }

  form: FormGroup;

  submitted = false;

  messenger: string;
  quoteId: string;
  visibility = false;
  fullName = localStorage.getItem('fullName');

  // tslint:disable-next-line: member-ordering
  shipment: any;
  createAlert = false;

  ngOnInit() {
    this.form = this.formBuilder.group({
      emailSender: ['', [Validators.required, Validators.email]],
      nameSender: ['', [Validators.required]],
      phoneSender: ['', [Validators.required]],
      addressSender: ['', [Validators.required]],
      localitySender: ['', [Validators.required]],
      postalSender: ['', [Validators.required]],
      countrySender: ['FR', [Validators.required]],
      emailRecv: ['', [Validators.required, Validators.email]],
      nameRecv: ['', [Validators.required]],
      phoneRecv: ['', [Validators.required]],
      addressRecv: ['', [Validators.required]],
      localityRecv: ['', [Validators.required]],
      postalRecv: ['', [Validators.required]],
      countryRecv: ['FR', [Validators.required]],
      packageLength: ['', [Validators.required]],
      packageWidth: ['', [Validators.required]],
      packageWeight: ['', [Validators.required]],
      packageHeight: ['', [Validators.required]],
      packageUnit: ['g', [Validators.required]],
      packageUnitCm: ['cm', [Validators.required]],

    });
  }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    } else {
      this.shipment = {
        data: {
          quote: {
            id: '',
          },
          cost: '',
          origin: {
            contact: {
              name: this.form.get('nameSender').value,
              email: this.form.get('emailSender').value,
              phone: this.form.get('phoneSender').value,
            },
            address: {
              country_code: this.form.get('countrySender').value,
              locality: this.form.get('localitySender').value,
              postal_code: this.form.get('postalSender').value,
              address_line1: this.form.get('addressSender').value
            }
          },
          destination: {
            contact: {
              name: this.form.get('nameRecv').value,
              email: this.form.get('emailRecv').value,
              phone: this.form.get('phoneRecv').value
            },
            address: {
              country_code: this.form.get('countryRecv').value,
              locality: this.form.get('localityRecv').value,
              postal_code: this.form.get('postalRecv').value,
              address_line1: this.form.get('addressRecv').value
            }
          },
          package: {
            dimensions: {
              height: this.form.get('packageHeight').value,
              width: this.form.get('packageWidth').value,
              length: this.form.get('packageLength').value,
              unit: 'cm'
            },
            grossWeight: {
              amount: this.form.get('packageWeight').value,
              unit: 'g'
            }
          }
        }
      };
      this.shipmentService.getQuote(this.shipment).subscribe((res: any) => {
        this.shipment.data.cost = res.data[0].amount;
        this.shipment.data.quote.id = res.data[0].id;
      });
      this.visibility = true;
    }
  }

  onCreate() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.addShipment(this.shipment);
    this.createAlert = true;
  }

  onBack(){
    this.visibility = false;
    this.createAlert = false;
  }

  addShipment(shipment) {
    this.shipmentService.addShipment(shipment).subscribe((res: any) => {
    });
  }
}



