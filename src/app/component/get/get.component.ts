import { ShipmentService } from './../../shipment.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ReactiveFormsModule
} from '@angular/forms';

@Component({
  selector: 'app-get',
  templateUrl: './get.component.html',
  styleUrls: ['./get.component.css']
})
export class GetComponent implements OnInit {
    get f() { return this.form.controls; }

  constructor(
    private formBuilder: FormBuilder,
    private shipmentService: ShipmentService,
    private route: ActivatedRoute
  ) {}
  edit = false;
  newShipment: any;
  shipment: any;
  data: any;
  ref: string;
  form: FormGroup;
  submitted = false;
  aftersave = true;
  base_url = 'http://localhost:3000';

  toggleEdit() {
    this.edit = !this.edit;
  }

  ngOnInit() {
    this.shipment = this.data;
    this.route.params.subscribe(params => {
      this.ref = params.id;
      this.shipmentService.getShipmentById(this.ref).subscribe((res: any) => {
        this.shipment = res.data;
      });
    });

    this.form = this.formBuilder.group({
      emailSender: ['', [Validators.required, Validators.email]],
      nameSender: ['', [Validators.required]],
      phoneSender: ['', [Validators.required]],
      addressSender: ['', [Validators.required]],
      localitySender: ['', [Validators.required]],
      postalSender: ['', [Validators.required]],
      emailRecv: ['', [Validators.required, Validators.email]],
      nameRecv: ['', [Validators.required]],
      phoneRecv: ['', [Validators.required]],
      addressRecv: ['', [Validators.required]],
      localityRecv: ['', [Validators.required]],
      postalRecv: ['', [Validators.required]],
    });


  }

  onSave() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    } else {
    this.newShipment = {
      data: {
        quote: {
          id: this.shipment.quote.id
        },
        cost: this.shipment.cost,
        origin: {
          contact: {
            name: this.form.get('nameSender').value,
            email: this.form.get('emailSender').value,
            phone: this.form.get('phoneSender').value
          },
          address: {
            locality: this.form.get('localitySender').value,
            postal_code: this.form.get('postalSender').value,
            address_line1: this.form.get('addressSender').value,
            country_code: 'FR'
          }
        },
        destination: {
          contact: {
            name: this.form.get('nameRecv').value,
            email: this.form.get('emailRecv').value,
            phone: this.form.get('phoneRecv').value
          },
          address: {
            locality: this.form.get('localityRecv').value,
            postal_code: this.form.get('postalRecv').value,
            address_line1: this.form.get('addressRecv').value,
            country_code: 'FR'
          }
        }
      }
    };
    this.shipmentService
      .updateShipment(this.ref, this.newShipment)
      .subscribe((res: any) => {
          });
    this.aftersave = !this.aftersave;
    }
  }

  onCancel() {
    this.edit = !this.edit;
  }
}
