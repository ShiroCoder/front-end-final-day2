import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ShipmentService } from './../../shipment.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  displayedColumns: string[] = ['ref', 'cost', 'created_at', 'sender', 'receiver', 'option'];
  dataSource: any;
  deteledShipment: any;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private formBuilder: FormBuilder,
    private shipmentService: ShipmentService,
    private router: Router
  ) {}

  ngOnInit() {
    this.shipmentService.getShipment().subscribe((res: any) => {
      this.dataSource = new MatTableDataSource(res.data);
      this.dataSource.paginator = this.paginator;
    });


  }
  confirmDelete(id) {
    this.deteledShipment = id;
  }

  onDelete() {
    console.log();
    this.shipmentService
      .deleteShipment(this.deteledShipment)
      .subscribe((res: any) => {
        this.router.navigateByUrl('/add');
      });
  }

  onDetail(id) {
    this.shipmentService.getShipmentById(id);
  }
}
export interface PeriodicElement {
  ref: string;
  cost: number;
  sender: string;
  receiver: string;
  created_at: string;
  option: string;
}

