import {
  CustomValidators,
  ConfirmValidParentMatcher,
  regExps,
  errorMessages } from './../services/validators.service';
import { ShipmentService } from './../shipment.service';
import { Component, OnInit } from '@angular/core';
import {
  NgForm,
  FormGroupDirective,
  FormControl,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';


// import { ErrorStateMatcher } from '@angular/material/core';


// export class MyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//     const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
//     const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

//     return (invalidCtrl || invalidParent);
//   }
// }


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private shipmentService: ShipmentService,
    private router: Router
  ) {
    this.createForm();
  }
  confirmValidParentMatcher = new ConfirmValidParentMatcher();
  message: string;
  registrationFailed = false;
  registrationSuccsessfully = false;
  errors = errorMessages;
  userRegistrationForm: FormGroup;
  userdata: any;
  submitted = false;
  // matcher = new MyErrorStateMatcher();
  createForm() {
    this.userRegistrationForm = this.formBuilder.group({
      fullName: [
        '',
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(128)
        ]
      ],
      emailGroup: this.formBuilder.group(
        {
          email: ['', [Validators.required, Validators.email]],
          confirmEmail: ['', Validators.required]
        },
        { validator: CustomValidators.childrenEqual }
      ),
      passwordGroup: this.formBuilder.group(
        {
          password: [
            '',
            [Validators.required, Validators.pattern(regExps.password)]
          ],
          confirmPassword: ['', Validators.required]
        },
        { validator: CustomValidators.childrenEqual }
      )
    });
  }
  ngOnInit() {
    // this.registerForm = this.formBuilder.group(
    //   {
    //     email: ["", [Validators.required, Validators.email]],
    //     password: ["", [Validators.required, Validators.minLength(6)]],
    //     confirmpassword: [""],
    //     fullname: ["", [Validators.required]]
    //   },
    //   { validator: this.checkPasswords }
    // );
  }
  get f() {
    return this.userRegistrationForm.controls;
  }

  onSubmit() {

    // stop here if form is invalid

    this.userdata = {
      email: this.userRegistrationForm.get('emailGroup.email').value,
      fullName: this.userRegistrationForm.get('fullName').value,
      password: this.userRegistrationForm.get('passwordGroup.password').value
    };
    this.shipmentService.register(this.userdata).subscribe(
      () => {
        console.log('registration ok');
        this.registrationSuccsessfully = true;
        this.message = 'Registration successfully!';
        this.router.navigate(['/login']);
      },
      error => {
        console.log(error);
        this.registrationFailed = true;
        this.message = 'Registration failed! Email has been used';
      }
    );
  }

  // checkPasswords(group: FormGroup) {
  //   // here we have the 'passwords' group
  //   let pass = group.controls.password.value;
  //   let confirmPass = group.controls.confirmpassword.value;

  //   return pass === confirmPass ? null : { notSame: true };
  // }
}
