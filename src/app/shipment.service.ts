import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ShipmentService {
  url = 'http://localhost:3000';

  constructor(  private http: HttpClient) { }

  getQuote(quote) {
    return this.http.post(`${this.url}/getquote`, quote);
  }

  getShipment() {
    return this.http.get(`${this.url}/getshipment`);
  }

  getShipmentById(id) {
    return this.http.get(`${this.url}/getshipmentdetail/${id}`);
  }

  addShipment(shipment) {
    return this.http.post(`${this.url}/createshipment`, shipment);
  }

  updateShipment(id,shipment){
    return this.http.patch(`${this.url}/updateshipment/${id}`, shipment);
  }

  deleteShipment(id) {
    return this.http.delete(`${this.url}/deleteshipment/${id}`);
  }

  sign_in(user){
    return this.http.post(`${this.url}/auth/sign_in`, user);
  }
  register(user){
    return this.http.post(`${this.url}/auth/register`, user);
  }
}
